# TestBeamAnalysisHistograms

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
This is a package for analysis of the ZDC 2022/2023 Test Beam Data, based on a [tutorial](https://ipnp.cz/scheirich/?page_id=174) created by D. Scheirich

## Description
This package is useful for extracting relevent quantities from ZDC TestBeam nTuples.  The output is a root file of histograms, which contain relevent quantities for our analysis.

## Installation
```
git clone https://gitlab.cern.ch/mhoppesc/testbeamanalysishistograms.git
git checkout basicFramework
make
python3 runMe.py
```

## Usage
Coming Soon
