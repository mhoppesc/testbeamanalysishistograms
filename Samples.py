from EventLoop import EventLoop

# -----------------------------------


class SampleElectron(EventLoop):
 """ event loop over the gluon-gluon fusion sample
 """

 def __init__(self):
  # call the inherited constructor
  EventLoop.__init__(self, "SampleElectronRun")

  # add the ggH samples into the event loop
  self.eventLoop.inputFiles.push_back('testBeamData/out_00095602_01.root')

# -----------------------------------
