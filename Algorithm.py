from ROOT import Algorithm as AlgorithmCpp
from ROOT import TFile
from ROOT import TMath
import numpy as np


class Algorithm(object):
 """ Wrapper around the c++ Algorithm class
 """

 def __init__(self, name, listHistVecs):
  self.name = name
  self.listHistVecs = listHistVecs
  self.alg = AlgorithmCpp()

 def setXLog(self, hist):
  axis = hist.GetXaxis()
  bins = axis.GetNbins()
  From = axis.GetXmin()
  To = axis.GetXmax()
  width = (To - From) / bins
  newbins = np.zeros(bins+1)
  for i in np.arange(bins+1):
    newbins[i] = TMath.Power(10, From + i*width)
  axis.Set(bins, newbins)

 def save(self, prefix):
  """ Saves the histograms into the output file.
  """
  # save everything into the ROOT file.
  f = TFile.Open("histogramsOutput/histograms.{}.{}.root".format(prefix, self.name), "recreate")
  f.cd()

  # printout
  print( "Histograms saved into file: {}".format(f.GetName()))

  # Loop throught all attributes of self.alg class
  # and save all classes that have "Write" method
  for attrName in dir(self.alg):
   attr = getattr(self.alg, attrName)
   if hasattr(attr, "Write"):
    attr.Write()
   elif attrName in self.listHistVecs:
    for hist in attr:
     hist.Write()
  
  print("file closed")
  # close the file
  f.Close()
