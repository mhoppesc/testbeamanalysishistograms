from Algorithm import Algorithm
from ROOT import TH1D

# -----------------------------------
class AlgDefault(Algorithm):
 """ Default set of histograms
 """
 def __init__(self):
  # call inherited constructor
  # list enumerates all your vectors of histograms
  Algorithm.__init__(self, "AlgDefault", ['v_h_ZDCMaxAdc','v_h_ZDCMaxSample'])
  
  
  nBins = 50
  # if range Min and Max both set to zero, histogram will automatically write to size of tree data it reads
  rangeMin = 0
  rangeMax = 0
  # create histograms
  self.alg.h_myFirstPlot = TH1D("runNumber", ":runNumber;Counts", nBins, rangeMin, rangeMax)
  
  #create vectors of histograms
  for zdcCounter in range(8):
    self.alg.v_h_ZDCMaxAdc[zdcCounter] = TH1D("ZDCLowGainCh{n}_maxAdc".format(n = zdcCounter), ";MaxZDCSingal [ADC];counts".format(n=zdcCounter),nBins,rangeMin,rangeMax)
    self.alg.v_h_ZDCMaxSample[zdcCounter] = TH1D("ZDCLowGainCh{n}_maxSample".format(n = zdcCounter), ";MaxZDCSample [ADC];counts".format(n=zdcCounter),nBins,rangeMin,rangeMax)
