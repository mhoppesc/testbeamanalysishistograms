//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jun 19 17:45:06 2023 by ROOT version 6.28/03
// from TTree run3RawTree/run3RawTree
// found on file: out_00095602_01.root
//////////////////////////////////////////////////////////

#ifndef RUN3RAWTREE_h
#define RUN3RAWTREE_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class run3RawTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           runNumber;
   Int_t           eventIndex;
   Double_t        eventTime;
   UInt_t          eventNumber;
   Int_t           lumiBlock;
   Int_t           BCID;
   Bool_t          goodTrigger;
   Bool_t          pileupPMT;
   Bool_t          pileupZDC;
   std::vector<unsigned int> *ZDC_chanStatus;
   std::vector<unsigned int> *ZDC_preSamples;
   std::vector<int>     *ZDC_chanMaxADC;
   std::vector<int>     *ZDC_chanMaxSample;
   std::vector<int>     *ZDC_chanSumADC;
   std::vector<float>   *ZDC_chanAvgADC;
   std::vector<float>   *ZDC_chanStdevADC;
   std::vector<float>   *ZDC_chanAvgNonAnaADC;
   std::vector<float>   *ZDC_chanStdevNonAnaADC;
   std::vector<int>     *ZDC_chanMaxAnaADC;
   std::vector<int>     *ZDC_chanMaxAnaSample;
   std::vector<int>     *ZDC_chanSumAnaADC;
   std::vector<int>     *ZDC_chanMinDer2ndAna;
   std::vector<int>     *ZDC_chanMinDer2ndDSAna;
   std::vector<std::vector<unsigned int> > *ZDC_dataSubtr;
   std::vector<std::vector<int> > *ZDC_deriv2nd;
   std::vector<std::vector<int> > *ZDC_deriv2ndDouble;
   std::vector<std::vector<unsigned int> > *ZDC_peakSamples;
   std::vector<std::vector<int> > *ZDC_peakValues;
   std::vector<unsigned int> *ZDC_NInTimePulse;
   std::vector<unsigned int> *ZDC_trigAmp;
   std::vector<float>   *ZDC_fitAmp;
   std::vector<float>   *ZDC_fitT0;
   std::vector<float>   *ZDC_fitTau1;
   std::vector<float>   *ZDC_fitTau2;
   std::vector<float>   *ZDC_fitC;
   std::vector<float>   *ZDC_fitChisq;
   std::vector<unsigned int> *PMT_chanStatus;
   std::vector<unsigned int> *PMT_preSamples;
   std::vector<int>     *PMT_chanMaxADC;
   std::vector<int>     *PMT_chanMaxSample;
   std::vector<int>     *PMT_chanSumADC;
   std::vector<float>   *PMT_chanAvgADC;
   std::vector<float>   *PMT_chanStdevADC;
   std::vector<float>   *PMT_chanAvgNonAnaADC;
   std::vector<float>   *PMT_chanStdevNonAnaADC;
   std::vector<int>     *PMT_chanMaxAnaADC;
   std::vector<int>     *PMT_chanMaxAnaSample;
   std::vector<int>     *PMT_chanSumAnaADC;
   std::vector<int>     *PMT_chanMinDer2ndAna;
   std::vector<int>     *PMT_chanMinDer2ndDSAna;
   std::vector<std::vector<unsigned int> > *PMT_dataSubtr;
   std::vector<std::vector<int> > *PMT_deriv2nd;
   std::vector<std::vector<int> > *PMT_deriv2ndDouble;
   std::vector<std::vector<unsigned int> > *PMT_peakSamples;
   std::vector<std::vector<int> > *PMT_peakValues;
   std::vector<unsigned int> *PMT_NInTimePulse;
   std::vector<unsigned int> *PMT_trigAmp;
   std::vector<float>   *PMT_fitAmp;
   std::vector<float>   *PMT_fitT0;
   std::vector<float>   *PMT_fitTau1;
   std::vector<float>   *PMT_fitTau2;
   std::vector<float>   *PMT_fitC;
   std::vector<float>   *PMT_fitChisq;
   std::vector<unsigned int> *RPD_chanStatus;
   std::vector<unsigned int> *RPD_preSamples;
   std::vector<int>     *RPD_chanMaxADC;
   std::vector<int>     *RPD_chanMaxSample;
   std::vector<int>     *RPD_chanSumADC;
   std::vector<float>   *RPD_chanAvgADC;
   std::vector<float>   *RPD_chanStdevADC;
   std::vector<float>   *RPD_chanAvgNonAnaADC;
   std::vector<float>   *RPD_chanStdevNonAnaADC;
   std::vector<int>     *RPD_chanMaxAnaADC;
   std::vector<int>     *RPD_chanMaxAnaSample;
   std::vector<int>     *RPD_chanSumAnaADC;
   std::vector<int>     *RPD_chanMinDer2ndAna;
   std::vector<int>     *RPD_chanMinDer2ndDSAna;
   std::vector<std::vector<unsigned int> > *RPD_dataSubtr;
   std::vector<std::vector<int> > *RPD_deriv2nd;
   std::vector<std::vector<int> > *RPD_deriv2ndDouble;
   std::vector<std::vector<unsigned int> > *RPD_peakSamples;
   std::vector<std::vector<int> > *RPD_peakValues;
   std::vector<unsigned int> *RPD_NInTimePulse;
   std::vector<unsigned int> *RPD_trigAmp;
   std::vector<float>   *RPD_fitAmp;
   std::vector<float>   *RPD_fitT0;
   std::vector<float>   *RPD_fitTau1;
   std::vector<float>   *RPD_fitTau2;
   std::vector<float>   *RPD_fitC;
   std::vector<float>   *RPD_fitChisq;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventIndex;   //!
   TBranch        *b_eventTime;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_goodTrigger;   //!
   TBranch        *b_pileupPMT;   //!
   TBranch        *b_pileupZDC;   //!
   TBranch        *b_ZDC_chanStatus;   //!
   TBranch        *b_ZDC_preSamples;   //!
   TBranch        *b_ZDC_chanMaxADC;   //!
   TBranch        *b_ZDC_chanMaxSample;   //!
   TBranch        *b_ZDC_chanSumADC;   //!
   TBranch        *b_ZDC_chanAvgADC;   //!
   TBranch        *b_ZDC_chanStdevADC;   //!
   TBranch        *b_ZDC_chanAvgNonAnaADC;   //!
   TBranch        *b_ZDC_chanStdevNonAnaADC;   //!
   TBranch        *b_ZDC_chanMaxAnaADC;   //!
   TBranch        *b_ZDC_chanMaxAnaSample;   //!
   TBranch        *b_ZDC_chanSumAnaADC;   //!
   TBranch        *b_ZDC_chanMinDer2ndAna;   //!
   TBranch        *b_ZDC_chanMinDer2ndDSAna;   //!
   TBranch        *b_ZDC_dataSubtr;   //!
   TBranch        *b_ZDC_deriv2nd;   //!
   TBranch        *b_ZDC_deriv2ndDouble;   //!
   TBranch        *b_ZDC_peakSamples;   //!
   TBranch        *b_ZDC_peakValues;   //!
   TBranch        *b_ZDC_NInTimePulse;   //!
   TBranch        *b_ZDC_trigAmp;   //!
   TBranch        *b_ZDC_fitAmp;   //!
   TBranch        *b_ZDC_fitT0;   //!
   TBranch        *b_ZDC_fitTau1;   //!
   TBranch        *b_ZDC_fitTau2;   //!
   TBranch        *b_ZDC_fitC;   //!
   TBranch        *b_ZDC_fitChisq;   //!
   TBranch        *b_PMT_chanStatus;   //!
   TBranch        *b_PMT_preSamples;   //!
   TBranch        *b_PMT_chanMaxADC;   //!
   TBranch        *b_PMT_chanMaxSample;   //!
   TBranch        *b_PMT_chanSumADC;   //!
   TBranch        *b_PMT_chanAvgADC;   //!
   TBranch        *b_PMT_chanStdevADC;   //!
   TBranch        *b_PMT_chanAvgNonAnaADC;   //!
   TBranch        *b_PMT_chanStdevNonAnaADC;   //!
   TBranch        *b_PMT_chanMaxAnaADC;   //!
   TBranch        *b_PMT_chanMaxAnaSample;   //!
   TBranch        *b_PMT_chanSumAnaADC;   //!
   TBranch        *b_PMT_chanMinDer2ndAna;   //!
   TBranch        *b_PMT_chanMinDer2ndDSAna;   //!
   TBranch        *b_PMT_dataSubtr;   //!
   TBranch        *b_PMT_deriv2nd;   //!
   TBranch        *b_PMT_deriv2ndDouble;   //!
   TBranch        *b_PMT_peakSamples;   //!
   TBranch        *b_PMT_peakValues;   //!
   TBranch        *b_PMT_NInTimePulse;   //!
   TBranch        *b_PMT_trigAmp;   //!
   TBranch        *b_PMT_fitAmp;   //!
   TBranch        *b_PMT_fitT0;   //!
   TBranch        *b_PMT_fitTau1;   //!
   TBranch        *b_PMT_fitTau2;   //!
   TBranch        *b_PMT_fitC;   //!
   TBranch        *b_PMT_fitChisq;   //!
   TBranch        *b_RPD_chanStatus;   //!
   TBranch        *b_RPD_preSamples;   //!
   TBranch        *b_RPD_chanMaxADC;   //!
   TBranch        *b_RPD_chanMaxSample;   //!
   TBranch        *b_RPD_chanSumADC;   //!
   TBranch        *b_RPD_chanAvgADC;   //!
   TBranch        *b_RPD_chanStdevADC;   //!
   TBranch        *b_RPD_chanAvgNonAnaADC;   //!
   TBranch        *b_RPD_chanStdevNonAnaADC;   //!
   TBranch        *b_RPD_chanMaxAnaADC;   //!
   TBranch        *b_RPD_chanMaxAnaSample;   //!
   TBranch        *b_RPD_chanSumAnaADC;   //!
   TBranch        *b_RPD_chanMinDer2ndAna;   //!
   TBranch        *b_RPD_chanMinDer2ndDSAna;   //!
   TBranch        *b_RPD_dataSubtr;   //!
   TBranch        *b_RPD_deriv2nd;   //!
   TBranch        *b_RPD_deriv2ndDouble;   //!
   TBranch        *b_RPD_peakSamples;   //!
   TBranch        *b_RPD_peakValues;   //!
   TBranch        *b_RPD_NInTimePulse;   //!
   TBranch        *b_RPD_trigAmp;   //!
   TBranch        *b_RPD_fitAmp;   //!
   TBranch        *b_RPD_fitT0;   //!
   TBranch        *b_RPD_fitTau1;   //!
   TBranch        *b_RPD_fitTau2;   //!
   TBranch        *b_RPD_fitC;   //!
   TBranch        *b_RPD_fitChisq;   //!

   run3RawTree(TTree *tree=0);
   virtual ~run3RawTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef run3RawTree_cpp
run3RawTree::run3RawTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("out_00095602_01.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("out_00095602_01.root");
      }
      f->GetObject("run3RawTree",tree);

   }
   Init(tree);
}

run3RawTree::~run3RawTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t run3RawTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t run3RawTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void run3RawTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   ZDC_chanStatus = 0;
   ZDC_preSamples = 0;
   ZDC_chanMaxADC = 0;
   ZDC_chanMaxSample = 0;
   ZDC_chanSumADC = 0;
   ZDC_chanAvgADC = 0;
   ZDC_chanStdevADC = 0;
   ZDC_chanAvgNonAnaADC = 0;
   ZDC_chanStdevNonAnaADC = 0;
   ZDC_chanMaxAnaADC = 0;
   ZDC_chanMaxAnaSample = 0;
   ZDC_chanSumAnaADC = 0;
   ZDC_chanMinDer2ndAna = 0;
   ZDC_chanMinDer2ndDSAna = 0;
   ZDC_dataSubtr = 0;
   ZDC_deriv2nd = 0;
   ZDC_deriv2ndDouble = 0;
   ZDC_peakSamples = 0;
   ZDC_peakValues = 0;
   ZDC_NInTimePulse = 0;
   ZDC_trigAmp = 0;
   ZDC_fitAmp = 0;
   ZDC_fitT0 = 0;
   ZDC_fitTau1 = 0;
   ZDC_fitTau2 = 0;
   ZDC_fitC = 0;
   ZDC_fitChisq = 0;
   PMT_chanStatus = 0;
   PMT_preSamples = 0;
   PMT_chanMaxADC = 0;
   PMT_chanMaxSample = 0;
   PMT_chanSumADC = 0;
   PMT_chanAvgADC = 0;
   PMT_chanStdevADC = 0;
   PMT_chanAvgNonAnaADC = 0;
   PMT_chanStdevNonAnaADC = 0;
   PMT_chanMaxAnaADC = 0;
   PMT_chanMaxAnaSample = 0;
   PMT_chanSumAnaADC = 0;
   PMT_chanMinDer2ndAna = 0;
   PMT_chanMinDer2ndDSAna = 0;
   PMT_dataSubtr = 0;
   PMT_deriv2nd = 0;
   PMT_deriv2ndDouble = 0;
   PMT_peakSamples = 0;
   PMT_peakValues = 0;
   PMT_NInTimePulse = 0;
   PMT_trigAmp = 0;
   PMT_fitAmp = 0;
   PMT_fitT0 = 0;
   PMT_fitTau1 = 0;
   PMT_fitTau2 = 0;
   PMT_fitC = 0;
   PMT_fitChisq = 0;
   RPD_chanStatus = 0;
   RPD_preSamples = 0;
   RPD_chanMaxADC = 0;
   RPD_chanMaxSample = 0;
   RPD_chanSumADC = 0;
   RPD_chanAvgADC = 0;
   RPD_chanStdevADC = 0;
   RPD_chanAvgNonAnaADC = 0;
   RPD_chanStdevNonAnaADC = 0;
   RPD_chanMaxAnaADC = 0;
   RPD_chanMaxAnaSample = 0;
   RPD_chanSumAnaADC = 0;
   RPD_chanMinDer2ndAna = 0;
   RPD_chanMinDer2ndDSAna = 0;
   RPD_dataSubtr = 0;
   RPD_deriv2nd = 0;
   RPD_deriv2ndDouble = 0;
   RPD_peakSamples = 0;
   RPD_peakValues = 0;
   RPD_NInTimePulse = 0;
   RPD_trigAmp = 0;
   RPD_fitAmp = 0;
   RPD_fitT0 = 0;
   RPD_fitTau1 = 0;
   RPD_fitTau2 = 0;
   RPD_fitC = 0;
   RPD_fitChisq = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventIndex", &eventIndex, &b_eventIndex);
   fChain->SetBranchAddress("eventTime", &eventTime, &b_eventTime);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
//    fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("goodTrigger", &goodTrigger, &b_goodTrigger);
   fChain->SetBranchAddress("pileupPMT", &pileupPMT, &b_pileupPMT);
   fChain->SetBranchAddress("pileupZDC", &pileupZDC, &b_pileupZDC);
   fChain->SetBranchAddress("ZDC_chanStatus", &ZDC_chanStatus, &b_ZDC_chanStatus);
   fChain->SetBranchAddress("ZDC_preSamples", &ZDC_preSamples, &b_ZDC_preSamples);
   fChain->SetBranchAddress("ZDC_chanMaxADC", &ZDC_chanMaxADC, &b_ZDC_chanMaxADC);
   fChain->SetBranchAddress("ZDC_chanMaxSample", &ZDC_chanMaxSample, &b_ZDC_chanMaxSample);
   fChain->SetBranchAddress("ZDC_chanSumADC", &ZDC_chanSumADC, &b_ZDC_chanSumADC);
   fChain->SetBranchAddress("ZDC_chanAvgADC", &ZDC_chanAvgADC, &b_ZDC_chanAvgADC);
   fChain->SetBranchAddress("ZDC_chanStdevADC", &ZDC_chanStdevADC, &b_ZDC_chanStdevADC);
   fChain->SetBranchAddress("ZDC_chanAvgNonAnaADC", &ZDC_chanAvgNonAnaADC, &b_ZDC_chanAvgNonAnaADC);
   fChain->SetBranchAddress("ZDC_chanStdevNonAnaADC", &ZDC_chanStdevNonAnaADC, &b_ZDC_chanStdevNonAnaADC);
   fChain->SetBranchAddress("ZDC_chanMaxAnaADC", &ZDC_chanMaxAnaADC, &b_ZDC_chanMaxAnaADC);
   fChain->SetBranchAddress("ZDC_chanMaxAnaSample", &ZDC_chanMaxAnaSample, &b_ZDC_chanMaxAnaSample);
   fChain->SetBranchAddress("ZDC_chanSumAnaADC", &ZDC_chanSumAnaADC, &b_ZDC_chanSumAnaADC);
   fChain->SetBranchAddress("ZDC_chanMinDer2ndAna", &ZDC_chanMinDer2ndAna, &b_ZDC_chanMinDer2ndAna);
   fChain->SetBranchAddress("ZDC_chanMinDer2ndDSAna", &ZDC_chanMinDer2ndDSAna, &b_ZDC_chanMinDer2ndDSAna);
   fChain->SetBranchAddress("ZDC_dataSubtr", &ZDC_dataSubtr, &b_ZDC_dataSubtr);
   fChain->SetBranchAddress("ZDC_deriv2nd", &ZDC_deriv2nd, &b_ZDC_deriv2nd);
   fChain->SetBranchAddress("ZDC_deriv2ndDouble", &ZDC_deriv2ndDouble, &b_ZDC_deriv2ndDouble);
   fChain->SetBranchAddress("ZDC_peakSamples", &ZDC_peakSamples, &b_ZDC_peakSamples);
   fChain->SetBranchAddress("ZDC_peakValues", &ZDC_peakValues, &b_ZDC_peakValues);
   fChain->SetBranchAddress("ZDC_NInTimePulse", &ZDC_NInTimePulse, &b_ZDC_NInTimePulse);
   fChain->SetBranchAddress("ZDC_trigAmp", &ZDC_trigAmp, &b_ZDC_trigAmp);
   fChain->SetBranchAddress("ZDC_fitAmp", &ZDC_fitAmp, &b_ZDC_fitAmp);
   fChain->SetBranchAddress("ZDC_fitT0", &ZDC_fitT0, &b_ZDC_fitT0);
   fChain->SetBranchAddress("ZDC_fitTau1", &ZDC_fitTau1, &b_ZDC_fitTau1);
   fChain->SetBranchAddress("ZDC_fitTau2", &ZDC_fitTau2, &b_ZDC_fitTau2);
   fChain->SetBranchAddress("ZDC_fitC", &ZDC_fitC, &b_ZDC_fitC);
   fChain->SetBranchAddress("ZDC_fitChisq", &ZDC_fitChisq, &b_ZDC_fitChisq);
   fChain->SetBranchAddress("PMT_chanStatus", &PMT_chanStatus, &b_PMT_chanStatus);
   fChain->SetBranchAddress("PMT_preSamples", &PMT_preSamples, &b_PMT_preSamples);
   fChain->SetBranchAddress("PMT_chanMaxADC", &PMT_chanMaxADC, &b_PMT_chanMaxADC);
   fChain->SetBranchAddress("PMT_chanMaxSample", &PMT_chanMaxSample, &b_PMT_chanMaxSample);
   fChain->SetBranchAddress("PMT_chanSumADC", &PMT_chanSumADC, &b_PMT_chanSumADC);
   fChain->SetBranchAddress("PMT_chanAvgADC", &PMT_chanAvgADC, &b_PMT_chanAvgADC);
   fChain->SetBranchAddress("PMT_chanStdevADC", &PMT_chanStdevADC, &b_PMT_chanStdevADC);
   fChain->SetBranchAddress("PMT_chanAvgNonAnaADC", &PMT_chanAvgNonAnaADC, &b_PMT_chanAvgNonAnaADC);
   fChain->SetBranchAddress("PMT_chanStdevNonAnaADC", &PMT_chanStdevNonAnaADC, &b_PMT_chanStdevNonAnaADC);
   fChain->SetBranchAddress("PMT_chanMaxAnaADC", &PMT_chanMaxAnaADC, &b_PMT_chanMaxAnaADC);
   fChain->SetBranchAddress("PMT_chanMaxAnaSample", &PMT_chanMaxAnaSample, &b_PMT_chanMaxAnaSample);
   fChain->SetBranchAddress("PMT_chanSumAnaADC", &PMT_chanSumAnaADC, &b_PMT_chanSumAnaADC);
   fChain->SetBranchAddress("PMT_chanMinDer2ndAna", &PMT_chanMinDer2ndAna, &b_PMT_chanMinDer2ndAna);
   fChain->SetBranchAddress("PMT_chanMinDer2ndDSAna", &PMT_chanMinDer2ndDSAna, &b_PMT_chanMinDer2ndDSAna);
   fChain->SetBranchAddress("PMT_dataSubtr", &PMT_dataSubtr, &b_PMT_dataSubtr);
   fChain->SetBranchAddress("PMT_deriv2nd", &PMT_deriv2nd, &b_PMT_deriv2nd);
   fChain->SetBranchAddress("PMT_deriv2ndDouble", &PMT_deriv2ndDouble, &b_PMT_deriv2ndDouble);
   fChain->SetBranchAddress("PMT_peakSamples", &PMT_peakSamples, &b_PMT_peakSamples);
   fChain->SetBranchAddress("PMT_peakValues", &PMT_peakValues, &b_PMT_peakValues);
   fChain->SetBranchAddress("PMT_NInTimePulse", &PMT_NInTimePulse, &b_PMT_NInTimePulse);
   fChain->SetBranchAddress("PMT_trigAmp", &PMT_trigAmp, &b_PMT_trigAmp);
   fChain->SetBranchAddress("PMT_fitAmp", &PMT_fitAmp, &b_PMT_fitAmp);
   fChain->SetBranchAddress("PMT_fitT0", &PMT_fitT0, &b_PMT_fitT0);
   fChain->SetBranchAddress("PMT_fitTau1", &PMT_fitTau1, &b_PMT_fitTau1);
   fChain->SetBranchAddress("PMT_fitTau2", &PMT_fitTau2, &b_PMT_fitTau2);
   fChain->SetBranchAddress("PMT_fitC", &PMT_fitC, &b_PMT_fitC);
   fChain->SetBranchAddress("PMT_fitChisq", &PMT_fitChisq, &b_PMT_fitChisq);
   fChain->SetBranchAddress("RPD_chanStatus", &RPD_chanStatus, &b_RPD_chanStatus);
   fChain->SetBranchAddress("RPD_preSamples", &RPD_preSamples, &b_RPD_preSamples);
   fChain->SetBranchAddress("RPD_chanMaxADC", &RPD_chanMaxADC, &b_RPD_chanMaxADC);
   fChain->SetBranchAddress("RPD_chanMaxSample", &RPD_chanMaxSample, &b_RPD_chanMaxSample);
   fChain->SetBranchAddress("RPD_chanSumADC", &RPD_chanSumADC, &b_RPD_chanSumADC);
   fChain->SetBranchAddress("RPD_chanAvgADC", &RPD_chanAvgADC, &b_RPD_chanAvgADC);
   fChain->SetBranchAddress("RPD_chanStdevADC", &RPD_chanStdevADC, &b_RPD_chanStdevADC);
   fChain->SetBranchAddress("RPD_chanAvgNonAnaADC", &RPD_chanAvgNonAnaADC, &b_RPD_chanAvgNonAnaADC);
   fChain->SetBranchAddress("RPD_chanStdevNonAnaADC", &RPD_chanStdevNonAnaADC, &b_RPD_chanStdevNonAnaADC);
   fChain->SetBranchAddress("RPD_chanMaxAnaADC", &RPD_chanMaxAnaADC, &b_RPD_chanMaxAnaADC);
   fChain->SetBranchAddress("RPD_chanMaxAnaSample", &RPD_chanMaxAnaSample, &b_RPD_chanMaxAnaSample);
   fChain->SetBranchAddress("RPD_chanSumAnaADC", &RPD_chanSumAnaADC, &b_RPD_chanSumAnaADC);
   fChain->SetBranchAddress("RPD_chanMinDer2ndAna", &RPD_chanMinDer2ndAna, &b_RPD_chanMinDer2ndAna);
   fChain->SetBranchAddress("RPD_chanMinDer2ndDSAna", &RPD_chanMinDer2ndDSAna, &b_RPD_chanMinDer2ndDSAna);
   fChain->SetBranchAddress("RPD_dataSubtr", &RPD_dataSubtr, &b_RPD_dataSubtr);
   fChain->SetBranchAddress("RPD_deriv2nd", &RPD_deriv2nd, &b_RPD_deriv2nd);
   fChain->SetBranchAddress("RPD_deriv2ndDouble", &RPD_deriv2ndDouble, &b_RPD_deriv2ndDouble);
   fChain->SetBranchAddress("RPD_peakSamples", &RPD_peakSamples, &b_RPD_peakSamples);
   fChain->SetBranchAddress("RPD_peakValues", &RPD_peakValues, &b_RPD_peakValues);
   fChain->SetBranchAddress("RPD_NInTimePulse", &RPD_NInTimePulse, &b_RPD_NInTimePulse);
   fChain->SetBranchAddress("RPD_trigAmp", &RPD_trigAmp, &b_RPD_trigAmp);
   fChain->SetBranchAddress("RPD_fitAmp", &RPD_fitAmp, &b_RPD_fitAmp);
   fChain->SetBranchAddress("RPD_fitT0", &RPD_fitT0, &b_RPD_fitT0);
   fChain->SetBranchAddress("RPD_fitTau1", &RPD_fitTau1, &b_RPD_fitTau1);
   fChain->SetBranchAddress("RPD_fitTau2", &RPD_fitTau2, &b_RPD_fitTau2);
   fChain->SetBranchAddress("RPD_fitC", &RPD_fitC, &b_RPD_fitC);
   fChain->SetBranchAddress("RPD_fitChisq", &RPD_fitChisq, &b_RPD_fitChisq);
   Notify();
}

Bool_t run3RawTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void run3RawTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t run3RawTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef run3RawTree_cxx
