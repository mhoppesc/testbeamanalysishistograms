#include "Data.h"

Data::Data(TTree *tree) : m_tree(tree)
{
  m_tree->SetBranchAddress("runNumber", &runNumber);
  m_tree->SetBranchAddress("ZDC_chanMaxADC", &ZDC_chanMaxADC);
  m_tree->SetBranchAddress("ZDC_chanMaxSample", &ZDC_chanMaxSample);
}
