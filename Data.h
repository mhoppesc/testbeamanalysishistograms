#ifndef DATA_H
#define DATA_H

#include "TTree.h"
#include "vector"

class Data
{
public:
  /**
   * @brief Construct a new Data object
   *
   * @param tree - pointer to the TTree (or TChain) class
   */
  Data(TTree *tree);
  /**
   * @brief Tree variables
   */
  
  int runNumber;
  std::vector<int> *ZDC_chanMaxADC=0;
  std::vector<int> *ZDC_chanMaxSample=0;
  
protected:
  /**
   * @brief pointer to the TTree (or TChain) class
   */
  TTree *m_tree = 0;
};

#endif
