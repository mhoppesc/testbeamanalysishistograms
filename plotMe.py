from Plot import Plot
from ROOT import TCanvas, TLegend, kRed, kGreen, kBlack

# set ATLAS plot style
from AtlasStyle import setStyle
setStyle()
kGreen = 416
kRed = 632
kBlue = 600
kMagenta = 616
kCyan = 432

# draw plots

histNames = ['UPC Sample Side A']

sampleNames = ['PbPb2018UPCwCalibNoCuts.AlgDefault']
canvases = []
plots = []
legends = []

color_list = [ kBlue, kMagenta+2, kCyan+3]
transperency = [.5,.5,.5,.5,.8]
for sampleName in sampleNames:

 # load individual plots
 plots_df = []

 for histName in histNames:
  plots_df.append( Plot("histogramsOutput/histograms.{}.root".format(sampleName), histName))

 # add plots from two samples into a single plot
 #plot_sf = Plot(plots_sf)
 #plot_df = Plot(plots_df)

 # set style
 #plot_sf.setStyleSolid(kRed)
 for i in range(len(histNames)):
  #plots_df[i].setStyleSolid(color_list[i],transperency[i])
  plots_df[i].setStyleMarker(color_list[i])
 # create a THStack


 plot = Plot( plots_df,True)

 # create a canvas
 c = TCanvas(sampleName, sampleName, 800, 600)
 gPad = c.cd(1)
 gPad.SetLogx()
 # draw
 plot.draw("nostack E1")

 # Create the same plot, but this time merging the histograms
 # We will set to the "errorbar style" and overlay the TH stack
 #plotErr = Plot([ plot_df])
 #plotErr.setStyleErrorbar(kBlack)
 #plotErr.draw("same")

 # draw legend
 legend = TLegend(.2, .7, .5, .9)
 #legend.AddEntry(plot_sf.hist, "SF leptons", "f")
 for i in range(len(histNames)):
    legend.AddEntry(plots_df[i].hist, histNames[i], "f")
 #legend.AddEntry(plotErr.hist, "Stat. uncertainty", "f")
 legend.Draw("same")



 # save the canvas
 c.Print("plotsOutput/{}.pdf".format(sampleName))

 # save the instances so they are not deleted by the garbage collector
 canvases += [c]
 plots += [plot]
 #legends += [legend]
