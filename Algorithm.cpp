#include "Algorithm.h"

Algorithm::Algorithm()
{
}

void Algorithm::initialize(Data *data)
{
  m_data = data;
}

void Algorithm::execute()
{

  // apply selection. Exit if didn't pass
  if (!passedSelection())
    return;

  // fill the plots
  fillPlots();
}

bool Algorithm::passedSelection()
{
  // select specific flavor combinations
  if (cut_select) // add your cut to this conditional statement
    return false;


  // passed all the cuts
  return true;
}

void Algorithm::fillPlots()
{
  // here we fill the histograms. We protect the code against the empty pointers.

  h_myFirstPlot.Fill(m_data->runNumber);
  for(int i = 0; i < m_data->ZDC_chanMaxADC->size(); i++)
  {
    v_h_ZDCMaxAdc.at(i).Fill(m_data->ZDC_chanMaxADC->at(i));
  }
  for (int i = 0; i < m_data->ZDC_chanMaxSample->size(); i++)
  {
    v_h_ZDCMaxSample.at(i).Fill(m_data->ZDC_chanMaxSample->at(i));
  }
}
